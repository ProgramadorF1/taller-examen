
package tlle;

import java.util.Scanner;


public class Tlle {


    public String getNombre() {
        return nombre;
    }


    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getTaller() {
        return taller;
    }

    public void setTaller(double taller) {
        this.taller = taller;
    }

    public double getExamen() {
        return examen;
    }

    public void setExamen(double examen) {
        this.examen = examen;
    }

    public double getNotaFinal() {
        return notaFinal;
    }

    public void setNotaFinal(double notaFinal) {
        this.notaFinal = notaFinal;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }
    
    public void Calculador (double t1, double t2, double t3){
        this.taller=(taller+t1+t2+t3)/3*(0.4);
    }
    
    public void calculador2(double e1, double e2){
        this.examen=(examen+e1+e2)/2*(0.6);
    }
    
    public void CalculadorF(double taller,double examen){
        this.notaFinal=taller+examen;
    }
    
    public String resultado(){
        
        if(this.notaFinal<=3.5){
            return"No paso la materia";
        }else{
            return"Aprovo la materia";
        }
    }
        
        private String nombre;
        private double taller;
        private double examen;
        private double notaFinal;
        private String concepto;
        
        public Tlle(String nombre){
            this.nombre = "";
            this.taller = 0;
            this.examen = 0;
            this.notaFinal = 0;
            this.concepto = "";
    }

    public static void main(String[] args) {
        
        Tlle Colegio = new Tlle ("");
        Scanner lee = new Scanner (System.in);
        double t1 = 0; 
        double t2 = 0;
        double t3 = 0;
        double e1 = 0;
        double e2 = 0;
        
        System.out.println("Ingresa notas");
        t1 = lee.nextDouble();
        t2 = lee.nextDouble();
        t3 = lee.nextDouble();
        Colegio.Calculador(t1, t2, t3);
        System.out.println(Colegio.getTaller());
        
        System.out.println("Ingresa examenes");
        e1 = lee.nextDouble();
        e2 = lee.nextDouble();
        Colegio.calculador2(e1, e2);
        
        System.out.println(Colegio.getExamen());
        
        Colegio.CalculadorF(Colegio.getTaller(), Colegio.getExamen());
        System.out.println("Nota final "+ Colegio.notaFinal);
        
        System.out.println(Colegio.resultado());
    }
    
}
